package it.univaq.disim.OOSD.ECOtoll.domain.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.TreeSet;

import it.univaq.disim.OOSD.ECOtoll.domain.*;
import it.univaq.disim.OOSD.ECOtoll.domain.impl.CaselloAutostradale;

public class Loader {

	private static BufferedReader bf;

	public static <T extends Autostrada> TreeSet<CaselloAutostradale<T>> inizializzaCaselli(T as) {
		
		
		try {
			String s;
			bf = new BufferedReader(new FileReader("res/autostrade/" + as.getNome() + ".txt"));
			TreeSet<CaselloAutostradale<T>> returnList = new TreeSet<CaselloAutostradale<T>>();
			while((s = bf.readLine()) != null) {
				String data[] = s.split("\t");
				CaselloAutostradale<T> c = new CaselloAutostradale<T>(Integer.parseInt(data[2]), data[1], Double.parseDouble(data[0]), as);
				//System.out.println("Casello " + c.toString() + "\n");
				returnList.add(c);
			}
			return returnList; 
		} catch (Exception e) {
			System.out.println("Errore caricamento caselli");
			e.printStackTrace();
		}
		return null;
		
	}
	
}
