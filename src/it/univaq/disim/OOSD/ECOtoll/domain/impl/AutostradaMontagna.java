package it.univaq.disim.OOSD.ECOtoll.domain.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import it.univaq.disim.OOSD.ECOtoll.domain.*;
import it.univaq.disim.OOSD.ECOtoll.domain.data.Loader;

public class AutostradaMontagna implements Autostrada {

	private HashMap<Classe, Double> tariffaUnitaria = null;
	private TreeSet<CaselloAutostradale<AutostradaMontagna>> listaCaselli = null;
	private String nome = null;
	private HashSet<Double> tariffeDefault = null;
	private int numGallerie;
	
	public AutostradaMontagna(String nome) {
		tariffeDefault = new HashSet<Double>();
		
		tariffeDefault.add(0.08547);
		tariffeDefault.add(0.08749);
		tariffeDefault.add(0.11414);
		tariffeDefault.add(0.17426);
		tariffeDefault.add(0.20629);
		
		tariffaUnitaria = new HashMap<Classe, Double>();
		setTariffaUnitaria(tariffeDefault);
		this.nome = nome;
		listaCaselli = Loader.inizializzaCaselli(this);
	}

	@Override
	public void setTariffaUnitaria(HashSet<Double> tariffe) {
		if(tariffe.size() != tariffeDefault.size()) return;
		Classe[] c = {Classe.A, Classe.B, Classe.C, Classe.D, Classe.E};
		int i = 0;
		Iterator<Double> it = tariffe.iterator();
		while (it.hasNext()) {
			tariffaUnitaria.put(c[i], it.next());
			i++;
		}
	}
	
	@Override
	public HashSet<Double> getTariffaUnitaria() {
		HashSet<Double> ret = new HashSet<Double>();
		for(Map.Entry<Classe, Double> obj : tariffaUnitaria.entrySet()) ret.add(obj.getValue());
		return ret;
	}

	@Override
	public Double getTariffaVeicolo(GenericVeicolo v) {
		//System.out.println("GETTARIFFAVEICOLO: " + v.getClasseVeicolo());
		//System.out.println("GETTARIFFAVEICOLO: " + tariffaUnitaria.get(v.getClasseVeicolo()));
		return tariffaUnitaria.get(v.getClasseVeicolo());
	}

	@Override
	public CaselloAutostradale<? extends Autostrada> getCaselloById(int ID) {
		for(CaselloAutostradale<AutostradaMontagna> casello : listaCaselli) {
			if(casello.getID() == ID) return casello;
		}
		return null;
	}

	@Override
	public String getNome() {
		return this.nome;
	}

	public int getNumGallerie() {
		return numGallerie;
	}

	public void setNumGallerie(int numGallerie) {
		this.numGallerie = numGallerie;
	}
	
	
	
}
