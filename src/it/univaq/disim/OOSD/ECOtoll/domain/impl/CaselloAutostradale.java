package it.univaq.disim.OOSD.ECOtoll.domain.impl;

import it.univaq.disim.OOSD.ECOtoll.domain.Autostrada;
import it.univaq.disim.OOSD.ECOtoll.domain.Casello;

public class CaselloAutostradale<T extends Autostrada> implements Comparable<Object>, Casello{
	
	private int ID;
	private String Nome;
	private double Km;
	private T a;
	
	public CaselloAutostradale(int ID, String Nome, double Km, T a) {
		this.ID = ID;
		this.Nome = Nome;
		this.Km = Km;
		this.setA(a);
	}
	
	public <V extends Casello> Double getDistanza(V c2) {
		//System.out.println("GETDISTANZA DEBUG: \nCasello1 : " + this.getNome() + " Casello2 : " + c2.getNome() + " Km  : " + Math.abs(c2.Km - this.Km));
		return Math.abs(c2.getKm() - this.getKm());
		
	}
	
	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public double getKm() {
		return Km;
	}

	public void setKm(double km) {
		Km = km;
	}

	public String toString() {
		return "Casello " + ID + " " + Nome + ", situato al Km " + Km + ".";
	}

	public T getA() {
		return a;
	}

	public void setA(T a) {
		this.a = a;
	}
	
	@Override
	public int compareTo(Object c2) {
		if(!(c2 instanceof CaselloAutostradale)) {
			System.out.println("Gli oggetti sono di tipo diverso");
			return -1;
		}
		CaselloAutostradale<T> c = (CaselloAutostradale<T>)c2;
		return Integer.compare(this.ID, c.ID);
	}
}
