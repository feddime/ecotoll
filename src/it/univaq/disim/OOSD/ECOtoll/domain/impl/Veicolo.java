package it.univaq.disim.OOSD.ECOtoll.domain.impl;

import it.univaq.disim.OOSD.ECOtoll.domain.Classe;
import it.univaq.disim.OOSD.ECOtoll.domain.GenericVeicolo;

public class Veicolo extends GenericVeicolo{
	
	Classe classe;
	
	public Veicolo(String modello, String marca, int anno, String targa, int assi, double peso, int altezza) {
		this.setModello(modello);
		this.marca = marca;
		this.anno = anno;
		this.targa = targa;
		this.assi = assi;
		this.peso = peso;
		this.altezza = altezza;
		this.classe = getClasseVeicolo();
	}
	
	public Classe getClasseVeicolo() {
		if(this.assi == 2 && this.altezza <= 130) return Classe.A;
		if(this.assi == 2 && this.altezza > 130) return Classe.B;
		if(this.assi == 3) return Classe.C;
		if(this.assi == 4) return Classe.D;
		if(this.assi > 4) return Classe.E;
		return Classe.NOT_SUPPORTED; 	// not supported
	}

}
