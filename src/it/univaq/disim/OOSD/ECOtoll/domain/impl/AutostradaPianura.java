package it.univaq.disim.OOSD.ECOtoll.domain.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

import it.univaq.disim.OOSD.ECOtoll.domain.Autostrada;
import it.univaq.disim.OOSD.ECOtoll.domain.Classe;
import it.univaq.disim.OOSD.ECOtoll.domain.GenericVeicolo;
import it.univaq.disim.OOSD.ECOtoll.domain.data.Loader;

public class AutostradaPianura implements Autostrada {

	private HashMap<Classe, Double> tariffaUnitaria = null;
	private TreeSet<CaselloAutostradale<AutostradaPianura>> listaCaselli = null;
	private String nome = null;
	private HashSet<Double> tariffeDefault = null;
	
	public AutostradaPianura(String nome) {
		tariffeDefault = new HashSet<Double>();
		
		tariffeDefault.add(0.07231);
		tariffeDefault.add(0.07401);
		tariffeDefault.add(0.09862);
		tariffeDefault.add(0.14864);
		tariffeDefault.add(0.17530);
		
		tariffaUnitaria = new HashMap<Classe, Double>();
		setTariffaUnitaria(tariffeDefault);
		this.nome = nome;
		listaCaselli = Loader.inizializzaCaselli(this);
	}

	@Override
	public void setTariffaUnitaria(HashSet<Double> tariffe) {
		if(tariffe.size() != tariffeDefault.size()) return;
		Classe[] c = {Classe.A, Classe.B, Classe.C, Classe.D, Classe.E};
		int i = 0;
		Iterator<Double> it = tariffe.iterator();
		while (it.hasNext()) {
			tariffaUnitaria.put(c[i], it.next());
			i++;
		}
	}
	
	@Override
	public HashSet<Double> getTariffaUnitaria() {
		HashSet<Double> ret = new HashSet<Double>();
		for(Map.Entry<Classe, Double> obj : tariffaUnitaria.entrySet()) ret.add(obj.getValue());
		return ret;
	}

	@Override
	public Double getTariffaVeicolo(GenericVeicolo v) {
		return tariffaUnitaria.get(v.getClasseVeicolo());
	}

	@Override
	public CaselloAutostradale<AutostradaPianura> getCaselloById(int ID) {
		for(CaselloAutostradale<AutostradaPianura> casello : listaCaselli) {
			if(casello.getID() == ID) return casello;
		}
		return null;
	}

	@Override
	public String getNome() {
		return this.nome;
	}
	
	
	
}
