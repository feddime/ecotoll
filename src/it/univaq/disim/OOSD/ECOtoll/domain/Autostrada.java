package it.univaq.disim.OOSD.ECOtoll.domain;

import java.util.HashSet;

import it.univaq.disim.OOSD.ECOtoll.domain.impl.CaselloAutostradale;

public interface Autostrada {

	//static Double[] tariffeUniPian = { 0.07231, 0.07401, 0.09862, 0.14864, 0.17530 };
	//static Double[] tariffeUniMont = { 0.08547, 0.08749, 0.11414, 0.17426, 0.20629 };

	public HashSet<Double> getTariffaUnitaria();

	public void setTariffaUnitaria(HashSet<Double> tariffaUnitaria);
	
	public Double getTariffaVeicolo(GenericVeicolo v);
	
	public CaselloAutostradale<? extends Autostrada> getCaselloById(int ID);
	
	public String getNome();
	
}
