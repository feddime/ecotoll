package it.univaq.disim.OOSD.ECOtoll.domain;

public interface Casello {
	
	public <T extends Casello> Double getDistanza(T c2);
	
	public double getKm();
	
	public int getID();
	
}
