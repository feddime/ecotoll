package it.univaq.disim.OOSD.ECOtoll.domain;

import it.univaq.disim.OOSD.ECOtoll.domain.impl.CaselloAutostradale;

public class Pedaggio {
	
	private static int IVA = 22;
	
	public static int getIVA() {
		return IVA;
	}

	public static void setIVA(int iVA) {
		IVA = iVA;
	}

	public static Double calcolaPedaggio(GenericVeicolo v, CaselloAutostradale<? extends Autostrada> c1, CaselloAutostradale<? extends Autostrada> c2) {
		
		if(!c1.getA().equals(c2.getA())) return null; 
		
		Double tariffa1 = c1.getA().getTariffaVeicolo(v);
		Double tariffa2 = c1.getDistanza(c2);
		
		Double tariffa = tariffa1 * tariffa2;
		
		tariffa += ( tariffa / 100 * IVA);
		
		
		tariffa = roundTen(tariffa);

		return tariffa;
		
	}

	private static double roundTen(double tariffa) {
		
		tariffa *= 100;
		
		int last = (int) Math.floor(tariffa) % 10;
		
		int middle = (int) tariffa;
		
		if(last <= 2) {
			middle -= last;
		}
		else if(last >= 3 && last <= 7) {
			middle -= last;
			middle += 5;
		}
		else {
			middle -= last;
			middle += 10;
			
		}
		
		//System.out.println("last " + last + ", tariffa nuova: " + ( (double) middle ) / 10);
		double m2 = (double) middle;
		
		return m2 / 100 ;
	}
}
