package it.univaq.disim.OOSD.ECOtoll;

import java.util.HashSet;

import it.univaq.disim.OOSD.ECOtoll.domain.*;
import it.univaq.disim.OOSD.ECOtoll.domain.impl.AutostradaMontagna;
import it.univaq.disim.OOSD.ECOtoll.domain.impl.Veicolo;

public class Main {

	public static void main(String[] args) {
		GenericVeicolo v1 = new Veicolo("Giulietta", "Alfa Romeo", 2019, "XF394OP", 2, 1320, 147);
		GenericVeicolo v2 = new Veicolo("Punto", "FIAT", 2007, "JE746CG", 4, 2000, 137);
		
		Autostrada a1 = new AutostradaMontagna("A1 Milano-Napoli");
		System.out.println( Pedaggio.calcolaPedaggio(v2, a1.getCaselloById(37), a1.getCaselloById(6)) + " EURO");
		System.out.println( Pedaggio.calcolaPedaggio(v1, a1.getCaselloById(37), a1.getCaselloById(6)) + " EURO");
		
		// IN SEGUITO A CAMBIAMENTI RIGUARDANTI LA TARIFFA UNITARIA POSSIAMO SETTARLA IN QUESTO MODO
		HashSet<Double> tariffeNuove = new HashSet<Double>();
		tariffeNuove.add(0.023);
		tariffeNuove.add(0.0234);
		tariffeNuove.add(0.034);
		tariffeNuove.add(0.045);
		tariffeNuove.add(0.055);
		a1.setTariffaUnitaria(tariffeNuove);
		// ED EFFETTUARE IL RICALCOLO DEL PEDAGGIO
		System.out.println( "Calcolo pedaggio con nuove tariffe : " + Pedaggio.calcolaPedaggio(v1, a1.getCaselloById(37), a1.getCaselloById(6)) + " EURO");
	
		// RICALCOLO DEL PEDAGGIO IN SEGUITO A CAMBAIMENTI DI IVA (ESEMPIO 33%)
		Pedaggio.setIVA(33);
		System.out.println( "Calcolo pedaggio con IVA diversa : " + Pedaggio.calcolaPedaggio(v1, a1.getCaselloById(37), a1.getCaselloById(6)) + " EURO");
		
	}

}
